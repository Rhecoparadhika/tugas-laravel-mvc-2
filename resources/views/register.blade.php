<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="form.css">
    
        <title>Form</title>
    </head>
    <body>
        <div>
            <h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
        </div>
        <!-- membuat form -->
        <form action="welcome" method="post">
            @csrf
            <!-- Kotak Nama -->
            <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname"><br><br>
            <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname"><br><br>
            <!-- Gender -->
            <label for="gender">Gender:</label><br><br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label><br><br>
            <!-- Nationality -->
            <label for="nationality">Nationality:</label><br><br>
            <select id="nationality" name="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
            </select><br><br>
            <!-- Language -->
            <label for="language">Language Spoken:</label><br><br>
            <input type="checkbox" id="language1" name="language" value="indonesia">
            <label for="language1"> Bahasa Indonesia </label><br>
            <input type="checkbox" id="language2" name="language" value="english">
            <label for="language2"> English </label><br>
            <input type="checkbox" id="language3" name="language" value="other">
            <label for="language3"> Other </label><br><br>
            <!-- Bio -->
            <label for="bio">Bio:</label><br><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>
            <!-- Submit -->
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>